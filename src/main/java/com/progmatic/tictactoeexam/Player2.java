/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;

/**
 *
 * @author István
 */
public class Player2 extends AbstractPlayer {

    public Player2(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        try{
            
        if (b.hasWon(myType)==true) {
            Cell cell= new Cell(0, 0);
        } else {
            Cell cell = new Cell(b.emptyCells().get(0).getCol(), b.emptyCells().get(0).getRow());
            if (b.emptyCells().isEmpty()) {
                return null;
            } else {
                b.put(cell);
                return cell;
            }
        }
        }
        catch(CellException e){
            e.getMessage();
            return cell;
            
    }
}

}
