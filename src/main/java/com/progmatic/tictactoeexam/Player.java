/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author István
 */
public class Player extends AbstractPlayer {

    public Player(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        Cell cell = new Cell(b.emptyCells().get(0).getCol(), b.emptyCells().get(0).getRow());
        try {
            if(b.emptyCells().isEmpty()){
                return null;
            }else{
                b.put(cell);
                return cell;
            }
            
        } catch (CellException ex) {
            ex.getMessage();
            return cell;
        }
    }

}
