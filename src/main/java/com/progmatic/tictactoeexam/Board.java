/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author István
 */
public class Board implements com.progmatic.tictactoeexam.interfaces.Board {

    public Cell[][] tabla = new Cell[3][3];

    @Override
    public void put(Cell cell) throws CellException {
        try {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    
                    Cell c = new Cell(i, j);
                    tabla[i][j] = c;
                    if (tabla[i][j].getCellsPlayer() != PlayerType.EMPTY) {
                        CellException ex = new CellException(cell.getRow(), cell.getCol(), "Nem üres!");
                        throw ex;
                    }
                }
            }
        } catch (CellException e) {
            CellException exc = new CellException(cell.getRow(), cell.getCol(), "Hiba!");
            throw exc;
        }

    }

    @Override
    public boolean hasWon(PlayerType p) {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (tabla[i][j].getCellsPlayer().equals(tabla[i][j - 1].getCellsPlayer())
                        && tabla[i][j].getCellsPlayer().equals(tabla[i][j - 2].getCellsPlayer())) {
                    return true;
                } else if (tabla[i][j].getCellsPlayer().equals(tabla[i - 1][j].getCellsPlayer())
                        && tabla[i][j].getCellsPlayer().equals(tabla[i - 2][j].getCellsPlayer())) {
                    return true;
                } else if (tabla[i][j].getCellsPlayer().equals(tabla[i - 1][j - 1].getCellsPlayer())
                        && tabla[i][j].getCellsPlayer().equals(tabla[i - 2][j - 2].getCellsPlayer())) {
                    return true;
                } else if (tabla[1][1].getCellsPlayer().equals(tabla[0][3].getCellsPlayer())
                        && tabla[1][1].getCellsPlayer().equals(tabla[3][0].getCellsPlayer())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> l = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (tabla[i][j].getCellsPlayer() != PlayerType.O
                        || tabla[i][j].getCellsPlayer() != PlayerType.X) {
                } else {
                    l.add(tabla[i][j]);
                }
            }
        }
        return l;
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        try {
            return tabla[rowIdx][colIdx].getCellsPlayer();
        } catch (Exception e) {
            e.getMessage();
            return tabla[rowIdx][colIdx].getCellsPlayer();
        }
    }
}
